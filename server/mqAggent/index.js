const amqp = require("amqplib");

export async function provider() {
  try{
    const connection  = await amqp.connect("amqp://localhost:5672");
    const channel = await connection.createChannel();
    await channel.assertQueue("dataTransfer");
    return channel;
  }
  catch(err){
    console.log(err);
  }
};