const Joi = require('joi');

module.exports = (server) => {
  const gameType = require('./gameTypeHandler')();

  server.route({
    method: 'POST',
    path: '/api/v1/game',
    handler: gameType.addGameData,
    config: {
      validate: {
        payload: {
          userId: Joi.string().required(),
          gameId: Joi.string().required(),
          transactionId: Joi.string().required(),
          betAmount: Joi.number().required(),
          winAmount: Joi.number().required(),
        }
      }
    }
  })
}