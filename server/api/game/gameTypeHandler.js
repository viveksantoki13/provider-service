import { provider } from '../../mqAggent';
module.exports = () => {
  const gameType = {}
  gameType.addGameData = async (request) => {
    try{
      const channel = await provider();
      const result = channel.sendToQueue("dataTransfer",Buffer.from(JSON.stringify(request.payload)));
      if(result) {
        return {
          statusCode: 200,
          data: {
            message: 'Record inserted succ',
          },
        }
      }
    }
    catch(err){
      console.log(err);
    }
  }
  return gameType;
};
