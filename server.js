import Hapi from "hapi";
import { apiRoot } from './server/api';

const server = new Hapi.Server({ "host": "localhost", "port": 3001 })

server.start();

function init(server) {
  apiRoot(server);
}

init(server);
